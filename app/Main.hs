module Main where

import           RIO
import qualified RIO.ByteString                as BS
import qualified RIO.ByteString.Lazy           as LBS
import           RIO.Directory
import           RIO.FilePath
import           RIO.Process
import qualified RIO.Text                      as Text

import qualified Data.Aeson                    as JSON
import qualified Data.Aeson.Types              as JSON
                                                ( typeMismatch )
import           Data.Hashable                  ( Hashable )
import           GHC.Generics                   ( Generic )
import           Network.HTTP.Types             ( status200 )
import           Network.Wai.Middleware.Cors
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Middleware.Static
import           Network.Wai.Parse
import qualified System.Etc                    as Etc
import           Web.Scotty

import           Paths_rio_etc_scotty_template  ( getDataFileName )

--------------------------------------------------------------------------------

data Cmd
  = PrintConfig
  | RunMain
  deriving (Show, Eq, Generic)

instance Hashable Cmd

instance JSON.FromJSON Cmd where
  parseJSON json = case json of
    JSON.String cmdName
      | cmdName == "config" -> return PrintConfig
      | cmdName == "run"    -> return RunMain
      | otherwise -> JSON.typeMismatch ("Cmd (" <> Text.unpack cmdName <> ")") json
    _ -> JSON.typeMismatch "Cmd" json

instance JSON.ToJSON Cmd where
  toJSON cmd = case cmd of
    PrintConfig -> JSON.String "config"
    RunMain     -> JSON.String "run"

--------------------------------------------------------------------------------

type MonadMyApp env m = (MonadReader env m, HasLogFunc env, HasProcessContext env, MonadIO m, HasASConfig env)

data ASConfig = ASConfig {
  asPort    :: Int
, asUploads :: FilePath
} deriving (Eq, Show)

data MyApp = MyApp {
  asConf         :: !ASConfig
, logFunc        :: !LogFunc
, processContext :: !ProcessContext
}

instance HasLogFunc MyApp where
  logFuncL = lens logFunc (\x y -> x { logFunc = y })

instance HasProcessContext MyApp where
  processContextL = lens processContext (\x y -> x { processContext = y })

class HasASConfig a where
  asConfigL :: Lens' a ASConfig

instance HasASConfig MyApp where
  asConfigL = lens asConf (\x y -> x { asConf = y })

--------------------------------------------------------------------------------

runArtifactServer :: MonadMyApp env m => m ()
runArtifactServer = do
  ASConfig p d <- asks . view $ asConfigL
  liftIO $ scotty p $ do
    middleware logStdoutDev
    middleware simpleCors
    middleware $ staticPolicy (noDots >-> addBase d)

    get "/search/:infix" $ do
      beam <- param "infix"
      gs   <- listDirectory d
      let hs = filter (Text.isInfixOf beam) . map Text.pack $ gs
      json hs

    get "/get/:file" $ do
      beam <- param "file"
      setHeader "Content-Type" "application/x-tar"
      file (d </> beam)

    post "/upload" $ do
      fs <- files
      let fs' =
            [ (fieldName, fileName fi, fileContent fi) | (fieldName, fi) <- fs ]
      forM_ fs' $ \(_, fn, fc) -> writeFileBinary
        (d </> (Text.unpack . decodeUtf8With lenientDecode $ fn))
        (LBS.toStrict fc)

--------------------------------------------------------------------------------

main :: IO ()
main = do
  specPath   <- getDataFileName "spec.yaml"
  configSpec <- Etc.readConfigSpec (Text.pack specPath)

  Etc.reportEnvMisspellingWarnings configSpec

  (configFiles, _fileWarnings) <- Etc.resolveFiles configSpec
  (cmd        , configCli    ) <- Etc.resolveCommandCli configSpec
  configEnv                    <- Etc.resolveEnv configSpec

  let configDefault = Etc.resolveDefault configSpec

      config        = configDefault <> configFiles <> configEnv <> configCli

  logOptions     <- logOptionsHandle stdout True
  processContext <- mkDefaultProcessContext

  withLogFunc logOptions $ \logFunc -> case cmd of
    PrintConfig -> Etc.printPrettyConfig config

    RunMain     -> do
      p <- Etc.getConfigValue ["server", "port"] config
      d <- Etc.getConfigValue ["server", "uploads"] config
      runRIO (MyApp (ASConfig p d) logFunc processContext) runArtifactServer
